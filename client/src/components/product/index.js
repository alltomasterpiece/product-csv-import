import React, {Component} from 'react'
import './styles.css'
import StatusOk from '../../assets/img/ok.png'
import StatusError from '../../assets/img/cross.png'

class Product extends Component {
    constructor (props) {
        super(props);
        this.state = {
            dimensions: {},
            importStatus: false
        }
    }
    onImgLoad = ({target:img}) => {
        this.setState({dimensions:{height:img.offsetHeight, width:img.offsetWidth}});
        if(img.offSetHeight&&img.offsetWidth) {
            this.setState({importStatus:true})
        }
    }
    render() {
        const {id, name, url} = this.props
        const {width, height} = this.state.dimensions
        const {importStatus} = this.state;
        
        if (id&&name&&url&importStatus) {
            this.setState({importStatus: true})
        }
        
            return (
                <div className = "product-container">
                    <div className = "product-image-container">
                        <img className = "product-image" onLoad = {this.onImgLoad} alt = "product image is not imported" src = {url}/>
                    </div>
                    <div className = "product-detail-container">
                        <h5 className = "product-id">{id?id:'product not imported'}</h5>
                        <h5 className = "product-name">{name?name: 'product name is not imported'}</h5>
                        <h5 className = "product-dimensions">{width?width+'X'+height:"image is not imported"}</h5>
                    </div>
                    <div className = "status-image-container" >
                        <img className = "status-image" alt = "status" src ={width?StatusOk:StatusError}/>
                    </div>
                </div>
            );
    }
    
}
export default Product;