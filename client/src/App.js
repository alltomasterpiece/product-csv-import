import './App.css'
import React, {Component, createRef} from 'react'
import axios from 'axios'
import Product from './components/product/index'
import uploadButtonImg from './assets/img/upload.png'
import numberOneRed from './assets/img/number_1_red.png'
import numberTwoRed from './assets/img/number_2_red.png'
import numberOneBlue from './assets/img/number_1_blue.png'
import numberTwoBlue from './assets/img/number_2_blue.png'

 class App extends Component{
  constructor (props)  {
    super(props);
    this.state = {
      selectedFile: null,
      productData: null,
      step: 1
    };
    
    this.hiddenFileInput = createRef();
  }
  onFileChange = event => {
    this.setState({ selectedFile: event.target.files[0] });
  };
  onFileUpload = () => {
    const formData = new FormData();
    formData.append(
      "file",
      this.state.selectedFile
    );
    
    axios.post("http://localhost:8080/api/csv/upload", formData)
    .then(res => {
      
      this.setState({productData: res.data.data})
      this.setState({step: 2})
    })
  };
  ShowResult = () => {
    if (this.state.step === 1) {
      return (
        <div className = "mainContent">
          <div className = "uploadButtonContainer" onClick = {this.fileInputClicked}>
            <img className = "uploadButtonImg" src = {uploadButtonImg}/>
            <input className = "fileInput" type="file" ref = {this.hiddenFileInput} onChange={this.onFileChange} />
            <h3 className = "textUpload">upload csv</h3>
          </div>
          <h3>{this.state.selectedFile?this.state.selectedFile.name:''}</h3>
        </div>
      );
    }
    else {
      if (this.state.productData){
        const {productData} = this.state
        const productList = productData.map((productItem) => (
          <Product
            key = {productItem.id}
            id = {productItem.id}
            name = {productItem.name}
            url = {productItem.url}/>
        ))
        return (
          <div className = "product-list-container">
            <div className = "product-list">
              {productList}
            </div>
          </div>
        );
      }
    }
    
  };
  ShowUpload = () => {

  }
  fileInputClicked = event => {
    
    this.hiddenFileInput.current.click()
  }
  stepOne = () => {
    
    this.setState({step: 1})
  }
  stepTwo = () => {
    
    this.setState({step: 2})
  }
  render() {
    
    return (
      <div className="App">
        <header className="header">
        </header>
        <div className = "buttonContainer">
          <button className = "button-validate" onClick={this.onFileUpload}>Validate</button>
        </div>
        <div className = "step-display-container">
          <div className = "step-display">
            <div className = "step-one-container" onClick = {this.stepOne}>
              <img className = "circle-number-one" alt = "circle number image" src = {this.state.step===1?numberOneRed:numberOneBlue}></img>
              <h2 className = {this.state.step===1?'step-number-red':'step-number-blue'}> products</h2>
            </div>
            
            <div className = "step-one-container" onClick = {this.stepTwo}>
              <img className = "circle-number-one" alt = "circle number image" src = {this.state.step===2?numberTwoRed:numberTwoBlue}></img>
              <h2 className = {this.state.step===2?'step-number-red':'step-number-blue'}> results</h2>
            </div>
          </div>
        </div>
        {this.ShowResult()}
      </div>
    )
  }
  
}
export default App;

