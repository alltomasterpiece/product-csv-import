const express = require("express");
const router = express.Router();
const csvController = require("../controllers/upload/csv.controller");
const upload = require("../middleware/upload");

let routes = (app) => {
  router.post("/upload", upload.single("file"), csvController.upload);
  //router.get('/get', csvController.getResponse);
  router.get('/get', function (req, res) {
    res.send('<b>My</b> first express http server');
});
  app.use("/api/csv", router);
  
  
};
module.exports = routes;