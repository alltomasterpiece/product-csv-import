const fs = require("fs");
const csv = require("fast-csv");
const upload = async (req, res) => {
  try {
    if (req.file == undefined) {
      return res.status(400).send("Please upload a CSV file!");
    }

    let csvs = [];
    let path = __basedir + "/resources/static/assets/uploads/" + req.file.filename;
    fs.createReadStream(path)
      .pipe(csv.parse({headers: true, delimiter: ';'}))
      .on("error", (error) => {
        throw error.message;
      })
      .on("data", (row) => {
          //const obj = rowProcessor(row);
          //console.log(row);
        csvs.push(row);
      })
      .on("end", () => {
          //csvs.push({'product_id':'asd34d3423erse', 'name': 'prpductanaem', 'url': 'https://dfasdfasdfas.png'})
          console.log(csvs)
        //CsvDb.bulkCreate(csvs)
            res.status(200).send({
                data: csvs,
              message:
                "Uploaded the file successfully: " + req.file.originalname,
            });
          
      });
      
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: "Could not upload the file: " + req.file.originalname,
    });
  }
};
const getResponse = (res) => {
  res.status(200).send({
    message:"Uploaded the file successfully: " 
});
}



module.exports = {
  upload,
  getResponse
};